Usage
--------

flo-qt	-	FLO Core Wallet (Qt)
flo-cli	-	FLO command line execution
flod	-	FLO daemon
flo-tx	-	FLO transactions

### For Pre-compiled executable version of FLO :
1. Download the compressed file for respective OS in **bin/** directory.
2. Extract the files
3. To run the executable files use the following commands according to the use:

	`./flo-qt`

	`./flo-cli`
	
	`./flod`
	
	`./flo-tx`
	
4. For more details view the readme file in the extracted directory

### Compiling on Linux OS Server
Go to **flo** directory, and execute
   
	
 	cd depends/

	make HOST=x86_64-linux-gnu

	cd ..

	./autogen.sh

	./configure --prefix=`pwd`/depends/x86_64-linux-gnu

	make
	
For PCs on Linux, and execute
   
	
 	cd depends/

	make HOST=x86_64-pc-linux-gnu

	cd ..

	./autogen.sh

	./configure --prefix=`pwd`/depends/x86_64-pc-linux-gnu

	make	
To make with debuging in GDB 

	cd depends/

 	make clean HOST=x86_64-pc-linux-gnu
	
  	cd ..
   
	./autogen.sh
 
	./configure CXXFLAGS="-O0 -ggdb3" --prefix=`pwd`/depends/x86_64-pc-linux-gnu 

	make -j $(nproc)

### Details: For creating Pre-compiled executable binary files from source code :
The dependencies for creating pre-compiled binaries are present in **depends/** directory.

	cd depends/

To build dependencies for the current arch/OS (usually this gives error if architecture/OS is not specified in some versions. Better to specify arch/OS prefix explicitly):

	make

To build for another arch/OS:

	make HOST=<host-platform-triplet>
	
A prefix will be generated that's suitable for plugging into FLO's configure.
To build FLO binaries :
	
	cd ..
	./autogen.sh
	./configure --prefix=`pwd`/depends/<host-platform-triplet>
	make

Common `<host-platform-triplets>` for cross compilation are:

- `i686-w64-mingw32` for Win32
- `x86_64-w64-mingw32` for Win64
- `x86_64-apple-darwin11` for MacOSX
- `arm-linux-gnueabihf` for Linux ARM 32 bit
- `aarch64-linux-gnu` for Linux ARM 64 bit
- `x86_64-linux-gnu` for Linux-based OS 64-bit
- `x86_64-pc-linux-gnu` for Linux-based PC OS 64-bit

for more details on creating binaries: https://github.com/ranchimall/FLO-wallet-core/blob/flo-master/depends/README.md

Upgrades
--------

### Added 2 features to the FLO wallet :
1. CoinControlFIFO - selects the coins that were received first to be spent first (First-In-First-Out).
To enable it: add **CoinControlFIFO=1** in flo.conf (or) pass **-CoinControlFIFO** as cmdline arg 


2. SendChangeToBack - send the change back to the coin's original address
To enable it, add **SendChangeToBack=1** in flo.conf (or) pass **-SendChangeToBack** as cmdline arg

### Added Multi-Wallet support for Linux :
Multi-wallet support allows the user to run more than 1 wallet simultaneously.
The Multi-wallet executable file is located in **tmp/**
Copy the executable(binary) file to the flo binary files
To access multi-wallet run :

	./multiWallet -create [walletName]
	./multiWallet <walletName> <command> [option]

For more details run :

	./multiWallet -help





